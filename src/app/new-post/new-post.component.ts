import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Post } from '../shared/post.modal';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  @Output() newPost = new EventEmitter<Post>();

  title: string = '';
  description: string = '';
  postId = '';
  post!: Post;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
      this.http.get<Post>(
        `https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts/${params['id']}.json`)
        .subscribe(result => {
          this.post = result;
          this.title = this.post.title;
          this.description = this.post.description;
        })
    });
  }
  onSubmit()    {
    if(this.postId === undefined){
      const dateCreated = new Date().toDateString();
      const title = this.title;
      const description = this.description;
      const body = {dateCreated,  title, description};
      this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts.json', body).subscribe();
    }else{
      const dateCreated = new Date().toDateString();
      const title = this.title;
      const description = this.description;
      const body = {dateCreated,  title, description};
      this.http.put<Post>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts/${this.postId}.json`, body).subscribe();
    }
  }
}
