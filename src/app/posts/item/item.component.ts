import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../../shared/post.modal';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() post!: Post;
  ngOnInit(): void {

  }
}
