import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.modal';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(private http: HttpClient) { }
  posts!: Post[];

  ngOnInit(): void {
    this.http.get<{[id: string]: Post}>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result =>{
        if(result === null){
          return [];
        }

      return Object.keys(result).map(id =>{
        const postData = result[id];

        return new Post(
          id,
          postData.title,
          postData.dateCreated,
          postData.description,
        );
      });
    }))
      .subscribe(posts =>{
        this.posts = posts;
      });
  }

}
