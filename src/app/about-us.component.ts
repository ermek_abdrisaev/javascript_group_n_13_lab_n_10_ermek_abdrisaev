import { Component } from '@angular/core';

@Component({
  selector: 'app-about-us',
  template: `
    <h1>About us</h1>`,
  styles: [`h1{color: gray;}`]
})

export class AboutUsComponent {}
