import { Component, Input, OnInit, Output } from '@angular/core';
import { Post } from '../shared/post.modal';
import { ActivatedRoute, Params, Route, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;
  postId = '';
  title: string = '';
  description: string = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      this.postId = params['id'];
    });
    this.http.get<Post>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts/${this.postId}.json`).subscribe( result => {
      this.post = result;
    })
  }

  editPost() {
    const dateCreated = new Date().toDateString();
    const title = this.title;
    const description = this.description;
    const body = {dateCreated,  title, description};
    this.http.post(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts/${this.postId}.json`, body).subscribe( result =>{

    });
  }

  deletePost(){
    this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/posts/${this.postId}.json`).subscribe();
    void this.router.navigate(['posts']);
  }
}
