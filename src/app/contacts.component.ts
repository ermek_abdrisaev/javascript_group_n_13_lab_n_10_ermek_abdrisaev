import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  template: `
    <h1>Our contacts:</h1>
    <p>Phone number: <strong>0801 707899981</strong></p>
    <p>Email: <strong>blogs@post.org</strong></p>
  `
})

export class ContactsComponent {}
