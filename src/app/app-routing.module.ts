import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { NewPostComponent } from './new-post/new-post.component';
import { AboutUsComponent } from './about-us.component';
import { ContactsComponent } from './contacts.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'posts', component: HomeComponent},
  {path: 'posts/post-details/:id', component: PostDetailsComponent},
  {path: 'posts/post-details/:id/:edit', component: NewPostComponent},
  {path: 'posts/add', component: NewPostComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'contact', component: ContactsComponent},
  // {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
